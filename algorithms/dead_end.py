from collections import deque

def is_dead_end(maze, i, j, width):
	if(width - 1 > i and i > 0 and width - 1 > j and j > 0):
		return maze[i + 1][j] + maze[i - 1][j] + maze[i][j + 1] + maze[i][j - 1] <= 1.7
	else:
		True

def deadend_solve_maze(maze):
	width = maze.shape[0]

	dead_ends_list = []

	for i in range(1, width - 1):
		for j in range(1, width - 1):
			if(maze[i][j] == 1) and is_dead_end(maze, i, j, width):
				dead_ends_list.append([i, j])

	for pair in dead_ends_list:
		while is_dead_end(maze, pair[0], pair[1], width):
			maze[pair[0], pair[1]] = 0.2
			if(maze[pair[0] + 1, pair[1]] == 1):
				pair = [pair[0] + 1, pair[1]]
			elif(maze[pair[0] - 1, pair[1]] == 1):
				pair = [pair[0] - 1, pair[1]]
			elif(maze[pair[0], pair[1] + 1] == 1):
				pair = [pair[0], pair[1] + 1]
			elif(maze[pair[0], pair[1] - 1] == 1):
				pair = [pair[0], pair[1] - 1]

	return maze

