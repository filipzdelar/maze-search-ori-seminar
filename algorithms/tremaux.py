from collections import deque

def tremaux_solve_maze(maze):
    width = maze.shape[0]
    start_x = 0

    #find starting location
    for i in range(width):
        if(maze[0][i] == 1):
            start_x = i
            break


    queue = deque([])
    queue.append([0,start_x])

    while len(queue) != 0:
        current = queue.pop()
        maze[current[0]][current[1]] = 0.5
        
        if maze[current[0] - 1][current[1]] == 1 and current[0] - 1 != 0:
            queue.append([current[0] - 1,current[1]])
        
        if maze[current[0]][current[1] + 1] == 1:
            queue.append([current[0],current[1] + 1])
        
        if maze[current[0]][current[1] - 1] == 1:
            queue.append([current[0],current[1] - 1])

        if current[0] + 1 == width:
            break
        elif  maze[current[0] + 1][current[1]] == 1:
            queue.append([current[0] + 1,current[1]])
    
    return maze
