from collections import deque

def wallfollower_solve_maze(maze):
	width = maze.shape[0]
	start_x = 0
	end_nodes = []

	starting_look = {
		"down"  : 0,
		"right" : 1,
		"up"    : 2,
		"left"  : 3
	}


	for i in range(width):
		if(maze[0][i] == 1):
			start_x = i
			break
		   
	queue = deque([])
	queue.append([0, start_x])      

	moving_x = 1
	moving_y = start_x

	queue.append([1, start_x]) 
	step_look = "down"

	explored = []

	while  moving_y != width:		
		if(moving_x + 1 == width or moving_y + 1 == width):
			break
			
		for i in range(4):
			if ((starting_look[step_look] + i) % 4) == 0 and maze[moving_x - 1][moving_y] == 1:
				queue.append([moving_x - 1, moving_y])  
				moving_x -= 1 
				step_look = "left"
				break
			elif ((starting_look[step_look] + i) % 4) == 1 and maze[moving_x][moving_y + 1] == 1:
				queue.append([moving_x, moving_y + 1]) 
				moving_y += 1  
				step_look = "down"
				break
			elif ((starting_look[step_look] + i) % 4) == 2 and maze[moving_x + 1][moving_y] == 1:
				queue.append([moving_x + 1, moving_y])   
				moving_x += 1
				step_look = "right"
				break
			elif ((starting_look[step_look] + i) % 4) == 3 and maze[moving_x][moving_y - 1] == 1:
				queue.append([moving_x, moving_y - 1])  
				moving_y -= 1
				step_look = "up" 
				break

		if(len(queue)>2):
			if(queue[-3] == queue[-1]):
				current = queue.pop()
				explored.append(current)
				current = queue.pop()
				explored.append(current)

	return color_shortest_path(maze, queue, explored)


def color_shortest_path(maze,end_nodes,explored):   
	
	for tip in explored:
		maze[tip[0]][tip[1]] = 0.25
	for tup in end_nodes:
		maze[tup[0]][tup[1]] = 0.75

	return maze