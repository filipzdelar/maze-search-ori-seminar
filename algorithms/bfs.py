from collections import deque

def bfs_solve_maze(maze):
    width = maze.shape[0]
    start_x = 0

    #find starting location
    for i in range(width):
        if(maze[0][i] == 1):
            start_x = i
            break
    
    queue = deque([])
    queue.append([0,start_x,0])
    explored = []
    end_nodes = []
    
    #bfs
    while len(queue) != 0:
        current = queue.popleft()
        explored.append(current)
        level = current[2] + 1
        maze[current[0]][current[1]] = 0.5
        
        if current[0] + 1 == width:
            end_nodes.append([current[0],current[1],len(explored) -1])
        elif  maze[current[0] + 1][current[1]] == 1:
            queue.append([current[0] + 1,current[1],level])
        if maze[current[0]][current[1] + 1] == 1:
            queue.append([current[0],current[1] + 1,level])
        
        if maze[current[0]][current[1] - 1] == 1:
            queue.append([current[0],current[1] - 1,level])
        
        if maze[current[0] - 1][current[1]] == 1 and current[0] - 1 != 0:
            queue.append([current[0] - 1,current[1],level])

    return color_shortest_path(maze,end_nodes,explored)


def color_shortest_path(maze,end_nodes,explored):
    if len(end_nodes) == 0:
        return maze
    position = end_nodes[0][2]
    node = explored[position]
    currentlevel = node[2]
    while currentlevel != 0:
        maze[node[0]][node[1]] = 0.75
        currentlevel -= 1
        for x in explored:
            if x[2] == currentlevel:
                if x[0] == node[0]-1 and x[1] == node[1]:
                    node = x
                    break
                elif x[0] == node[0]+1 and x[1] == node[1]:
                    node = x
                    break
                elif x[1] == node[1]-1 and x[0] == node[0]:
                    node = x
                    break
                elif x[1] == node[1]+1 and x[0] == node[0]:
                    node = x
                    break
    
    maze[explored[0][0]][explored[0][1]] = 0.75
    return maze