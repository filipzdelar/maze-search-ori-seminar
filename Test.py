import numpy as np
import matplotlib.pyplot as plt
from algorithms.bfs import bfs_solve_maze
from algorithms.tremaux import tremaux_solve_maze
from algorithms.wall_follower import wallfollower_solve_maze 
from algorithms.dead_end import deadend_solve_maze
import matplotlib.cm as cm
import time


if __name__ == "__main__":
    maze25 = np.genfromtxt('25x25.csv',delimiter=',')
    maze101 = np.genfromtxt('101x101.csv',delimiter=',')
    maze201 = np.genfromtxt('201x201.csv',delimiter=',')
    
    
    ########### BFS
    # start_time = time.time()
    # sloved25 = bfs_solve_maze(maze25)
    # elapsed_time = time.time() - start_time
    # print("25 bfs time")
    # print(elapsed_time)

    # start_time = time.time()
    # sloved101 = bfs_solve_maze(maze101)
    # elapsed_time = time.time() - start_time
    # print("101 bfs time")
    # print(elapsed_time)

    # start_time = time.time()
    # sloved201 = bfs_solve_maze(maze201)
    # elapsed_time = time.time() - start_time
    # print("201 bfs time")
    # print(elapsed_time)


    # plt.imshow(sloved25)
    # plt.show()
    # plt.imshow(sloved101)
    # plt.show()
    # plt.imshow(sloved201)
    # plt.show()
    ############

    ############ tremaux
    start_time = time.time()
    sloved25 = tremaux_solve_maze(maze25)
    elapsed_time = time.time() - start_time
    print("25 tr time")
    print(elapsed_time)

    start_time = time.time()
    sloved101 = tremaux_solve_maze(maze101)
    elapsed_time = time.time() - start_time
    print("101 tr time")
    print(elapsed_time)

    start_time = time.time()
    sloved201 = tremaux_solve_maze(maze201)
    elapsed_time = time.time() - start_time
    print("201 tr time")
    print(elapsed_time)


    plt.imshow(sloved25)
    plt.show()
    plt.imshow(sloved101)
    plt.show()
    plt.imshow(sloved201)
    plt.show()
    ###########

    ############ wall follower
    """
    start_time = time.time()
    sloved25 = wallfollower_solve_maze(maze25)
    elapsed_time = time.time() - start_time
    print("25 bfs time")
    print(elapsed_time)

    start_time = time.time()
    sloved101 = wallfollower_solve_maze(maze101)
    elapsed_time = time.time() - start_time
    print("101 bfs time")
    print(elapsed_time)

    start_time = time.time()
    sloved201 = wallfollower_solve_maze(maze201)
    elapsed_time = time.time() - start_time
    print("201 bfs time")
    print(elapsed_time)


    plt.imshow(sloved25)
    plt.show()
    plt.imshow(sloved101)
    plt.show()
    plt.imshow(sloved201)
    plt.show()
    """
    ###########

    ############ dead endi
    """
    start_time = time.time()
    sloved25 = deadend_solve_maze(maze25)
    elapsed_time = time.time() - start_time
    print("25 bfs time")
    print(elapsed_time)

    start_time = time.time()
    sloved101 = deadend_solve_maze(maze101)
    elapsed_time = time.time() - start_time
    print("101 bfs time")
    print(elapsed_time)
    
    start_time = time.time()
    sloved201 = deadend_solve_maze(maze201)
    elapsed_time = time.time() - start_time
    print("201 bfs time")
    print(elapsed_time)
    

    plt.imshow(sloved25)
    plt.show()
    plt.imshow(sloved101)
    plt.show()
    plt.imshow(sloved201)
    plt.show()
    """

    #plt.imshow(maze25, interpolation='nearest', cmap=cm.Greys_r)
    #plt.show()
    # plt.imshow(maze101, interpolation='nearest', cmap=cm.Greys_r)
    # plt.show()
    #plt.imshow(maze201, interpolation='nearest', cmap=cm.Greys_r)
    #plt.show()
    #a = numpy.asarray(maze201)
    #numpy.savetxt(str(mazeSize)+"x"+str(mazeSize)+".csv", a, delimiter=",")
    

